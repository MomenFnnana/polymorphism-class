class Man {
    constructor(name, Religion) {
        this.name = name;
        this.Religion = Religion;
    }
    pray() {
        return '5 times pray ' + this.Religion
    }
}

class SpecificReligion extends Man {
    constructor(name, Religion, Religionless) {
        super(name, Religion)
        this.Religionless = Religionless;
    }
    pray() {
        console.log(super.pray());
        return `I am the ${this.name} of ${this.Religionless},just dont care the time! `
    }
}

const Christian = new SpecificReligion('muslim blhaweya', 'Muslim', 'pray 1 time');
Christian.pray()

